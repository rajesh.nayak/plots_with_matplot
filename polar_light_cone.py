import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import odeint

def rdot(xx,l, Et, Lz):
	r=xx[0]
	th=xx[0]
	drdl=np.sqrt(Et**2 - (Lz**2)/r/r)
	dthdl=Lz/r/r
	return np.array([drdl, dthdl])


fig = plt.figure(figsize=(11,11))
#ax = fig.add_subplot(111, projection='3d')
#plt.axis('off')
r=1.0
th=0.0
Np=80
dl=0.1
theta=np.linspace(0, 2*np.pi, Np)
r1=np.zeros(theta.shape)
th1=np.zeros(theta.shape)
x=r*np.cos(theta)
y=r*np.sin(theta)
px=np.cos(theta)
py=np.sin(theta)
Lz=x*py-y*px;
Et=np.sqrt(px*px+py*py)
t=Et
for ii in range(Np):
	xx=[r,th]
	rt=odeint(rdot,xx, [0, dl], args=(Et[ii], Lz[ii]))
	print(rt)
	#r1[ii]=rt[1,0]
#x1=r1*np.cos(theta)
#y1=r1*np.sin(theta)
#plt.plot(x1,y1)
#plt.show()	


