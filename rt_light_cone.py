import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from matplotlib import cm
Nr=6
r=np.linspace(1, 6,Nr)
m=1
slp=1- 2*m/r
slm=-slp
fig= plt.figure(figsize=(20,20))
ax = fig.add_subplot(111, projection='3d')
plt.axis('off')
theta=np.linspace(0, 2*np.pi, 20)

for ii in range(Nr) :
	t=np.linspace(0, 0.2, 20)
	yp=slp[ii]*t
	T1, R1 = np.meshgrid(theta, yp)
	X1 = R1 * np.cos(T1)
	Y1 = R1 * np.sin(T1)
	Z1=1/20/slp[ii]*np.sqrt(X1**2+Y1**2)
	ax.plot_wireframe(X1+r[ii],Y1,Z1, color='C1', alpha=0.4, linewidth=0.4, cmap=cm.coolwarm)
	ym=slm[ii]*t
	T2, R2 = np.meshgrid(theta, ym)
	X2 = R2 * np.cos(T2)
	Y2 = R2 * np.sin(T2)
	Z2=1/20/slm[ii]*np.sqrt(X2**2+Y2**2)
	ax.plot_wireframe(X2+r[ii],Y2,Z2, color='C2', alpha=0.4, linewidth=0.4, cmap=cm.coolwarm)
	#plt.plot(yp,t, 'C1', linewidth=3)
	#plt.plot(ym,t, 'C2', linewidth=3)
	ax.view_init(elev=23, azim=-62)
	
theta=np.linspace(0, 2*np.pi, 80)
for ii in range(Nr) :
	x=r[ii]*np.cos(theta)
	y=r[ii]*np.sin(theta)
	plt.plot(x,y, 'C6', linewidth=1, alpha=0.5)
x=2*np.cos(theta)
y=2*np.sin(theta)
plt.plot(x,y, 'k--', linewidth=1)

plt.xlim([0,r[-1]+1])
plt.ylim([0,r[-1]+1])
ax.set_zlim([-0.2, 0.2])
#plt.yticks([])
#plt.grid(axis='x', alpha=0.5)
#plt.xticks([1, 2,3, 4, 5,6], ['M', '2M', '3M','4M', '5M', '6M'])
#ax.text(1.8,-0.4,0, 'In Side Event Horizon', rotation=90, color='C2', size=18)

#ax.text(2.1,-0.45,0, 'Out Side Event Horizon', rotation=90, color='C1', size=18)
plt.savefig('light_cone_sch.png')
plt.show()
	