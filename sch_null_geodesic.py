import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import odeint
m=1
Nr=10
def rdtin(xx,t,b,m):
	# b is impact paramter
	r=xx[0]
	th=xx[1]
	fr=1-2*m/r
	drdt=np.sqrt(1 - (b**2)/r/r*fr)
	dthdt=-b*r/r*fr
	return np.array([drdt, dthdt])
def rdtout(xx,t,b,m):
	# b is impact paramter
	r=xx[0]
	th=xx[1]
	fr=1-2*m/r
	drdt=-np.sqrt(1 - (b**2)/r/r*fr)
	dthdt=-b*r/r*fr
	return np.array([drdt, dthdt])
fig= plt.figure(figsize=(11,11))
ax=plt.axes(projection = 'polar') 
t=np.linspace(0,0.3, 100)
rates=np.linspace(-30, 30, Nr)
solrp=np.zeros(Nr)
solthp=np.zeros(Nr)
solrm=np.zeros(Nr)
solthm=np.zeros(Nr)
ii=0
for rate in rates :
	r0=[2.5, 0.5]
	b=-(1-2*m/r0[0])/r0[0]/r0[0]*rate
	#print(b)
	rin=odeint(rdtin,r0, t, args=(b,m))
	rout=odeint(rdtout,r0, t, args=(b,m))
	solrp[ii]=rout[99,0]
	solthp[ii]=rout[99,1]
	solrm[ii]=rin[99,0]
	solthm[ii]=rin[99,1]
	plt.polar(rin[:,1],rin[:,0])
	plt.polar(rout[:,1],rout[:,0])
	
	ii+=1
#plt.polar(solthp, solrp)
#plt.polar(solthm, solrm)
ax.set_rmax(11)
plt.show()



