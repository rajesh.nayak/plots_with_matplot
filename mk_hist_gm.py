import numpy as np
import matplotlib.pyplot as plt
## www.iiserkol.ac.in/~rajesh/my_python.html
N=1000
sigma=1.0
xmax=4.0*sigma
mu=0.0
bins=30
#generating gaussian random noise 
r=np.random.randn(N)
x=np.linspace(-xmax,xmax,bins)
dx=x[1]-x[0]
# Guassian function
y=np.sqrt(np.pi/2)*np.sqrt(N)*(sigma*np.sqrt(2.0*np.pi))*np.exp(-0.5*((x-mu)/sigma)**2)
plt.hist(r, bins=x,  rwidth=0.9 , color='C3', label='histogram')
plt.plot(x+dx/2,y, 'C2o-',linewidth=3, mec='k', mfc='C9', label='Guassian')
plt.xlabel('x')
plt.ylabel('count')
plt.legend()
plt.title('Guassian distribution')
plt.grid(axis='y', linestyle=':')
plt.show()