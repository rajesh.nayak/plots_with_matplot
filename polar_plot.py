import numpy as np 
import matplotlib.pyplot as plt 

fig= plt.figure(figsize=(11,11))
plt.axes(projection = 'polar') 
plt.axis('off')

r=2
angle = np.arange(0, (2 * np.pi), 0.1) 
rad=np.linspace(2.0, 11, 10)
for x in angle :
	plt.polar(x*np.ones(rad.shape), rad , color='k', linewidth=0.5, alpha=0.4)
plt.polar(angle, r*np.ones(angle.shape), color='k', linewidth=0.5, alpha=0.4)
plt.savefig('bh_01.png')

r=2
angle = np.arange(0, (2 * np.pi), 0.01) 
rad=r*np.ones(angle.shape)
plt.polar(angle, rad, color='r', linewidth=3)
plt.text(-0.6, 1.1, 'r=2M',rotation=90,color='red' ,fontsize=18)
plt.savefig('bh_02.png')
r=3
angle = np.arange(0, (2 * np.pi), 0.01) 
rad=r*np.ones(angle.shape)
plt.polar(angle, rad,'-', color='C0', linewidth=1.5)
plt.text(-0.1, 3.1, 'r=3M',rotation=90,color='C0' ,fontsize=18)
plt.savefig('bh_03.png')
r=6
angle = np.arange(0, (2 * np.pi), 0.01) 
rad=r*np.ones(angle.shape)
plt.polar(angle, rad,'-', color='C1', linewidth=1.5)
plt.text(-0.0, 6.1, 'r=6M',rotation=90,color='C1' ,fontsize=18)
plt.savefig('bh_04.png')
r=10
angle = np.arange(0, (2 * np.pi), 0.01) 
rad=r*np.ones(angle.shape)
plt.polar(angle, rad,'-', color='C2', linewidth=1.5)
plt.text(-0.0, 10.1, 'r=10M',rotation=90,color='C2' ,fontsize=18)
plt.savefig('bh_05.png')




plt.show()