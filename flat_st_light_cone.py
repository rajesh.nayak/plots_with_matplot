import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


fig = plt.figure(figsize=(11,11))
#ax = fig.add_subplot(111, projection='3d')
plt.axis('off')
xmax=1
xmin=-xmax
Np=80
dl=1.0
theta=np.linspace(0, 2*np.pi, Np)
px=np.cos(theta)
py=np.sin(theta)
Et=np.sqrt(px*px+py*py)
t=Et
plt.plot(px, py, '-')
plt.show()

