import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


fig = plt.figure(figsize=(15,15))
ax = fig.add_subplot(111, projection='3d')
ax.axis('off')
xmax=1
xmin=-xmax
Np=30

l=np.linspace(xmin, xmax, Np)
x,y=np.meshgrid(l,l)

present=np.zeros(x.shape)
# plot z=0 plane
ax.plot_wireframe(x,y,present, alpha=0.5)

# plot axis lines
ax.plot([0], [0], [0], '*r')
ax.plot([-1.1, -0.5],[1.1, 1.1],[0,0],'k')
ax.plot([-1.1, -1.1],[1.1, 0.5],[0,0],'k')
ax.plot([-1.1, -1.1],[1.1, 1.1],[0,0.5],'k')
plt.savefig('light_cone_01')
# plot cone 
theta = np.linspace(0,2*np.pi,2*Np)
r = np.linspace(0,1,Np)
T, R = np.meshgrid(theta, r)
X = R * np.cos(T)
Y = R * np.sin(T)
Z = np.sqrt(X**2 + Y**2)
Z1 = 1.5*np.sqrt(X**2 + Y**2)

ax.plot_wireframe(X,Y,Z, color='C1', alpha=0.6)
ax.plot_wireframe(X,Y,-Z, color='C1', alpha=0.6)

plt.savefig('light_cone_02')
ax.plot_wireframe(X,Y,Z1, color='C2', alpha=0.5)
ax.plot_wireframe(X,Y,-Z1, color='C2', alpha=0.5)
ax.plot([0], [0], [0], '*r', markersize=20)

plt.savefig('light_cone_03')
#plt.show()

